package com.weather.model;

import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WeatherModelTest {
    @Test
    public void testWeatherModelHashCodeAndEquals() {
        Collection<WeatherModel> weatherModels = new HashSet<>();
        WeatherModel textWeatherModel = new WeatherModel("source_1", "2000-01-01", "2000-01-01", 0f, 0f, "London");
        assertFalse(weatherModels.contains(textWeatherModel));
        weatherModels.add(textWeatherModel);
        assertTrue(weatherModels.contains(textWeatherModel));

        weatherModels.add(new WeatherModel("source_1", "2000-01-01", "2000-01-01", 1f, 1f, "London"));
        weatherModels.add(new WeatherModel("source_2", "2000-01-01", "2000-01-01", 2f, 2f, "London"));
        weatherModels.add(new WeatherModel("source_2", "2000-01-01", "2000-01-01", 3f, 3f, "London"));
        weatherModels.add(new WeatherModel("source_1", "2000-01-02", "2000-01-01", 1f, 1f, "London"));
        weatherModels.add(new WeatherModel("source_2", "2000-01-02", "2000-01-01", 2f, 2f, "London"));
        weatherModels.add(new WeatherModel("source_1", "2000-01-02", "2000-01-02", 1f, 1f, "London"));
        weatherModels.add(new WeatherModel("source_2", "2000-01-02", "2000-01-02", 2f, 2f, "London"));

        assertTrue(weatherModels.size() == 6);
    }
}
