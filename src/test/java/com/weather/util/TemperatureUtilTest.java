package com.weather.util;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TemperatureUtilTest {
    @Test
    public void testFahrenheitToCelsius() {
        Float fahrenheit = 32f;
        Float celsius = TemperatureUtil.fahrenheitToCelsius(fahrenheit);
        assertTrue(celsius.equals(0f));
    }

    @Test
    public void testCelsiusToFahrenheit() {
        Float celsius = 0f;
        Float fahrenheit = TemperatureUtil.celsiusToFahrenheit(celsius);
        assertTrue(fahrenheit.equals(32f));
    }
}
