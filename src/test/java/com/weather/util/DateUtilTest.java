package com.weather.util;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DateUtilTest {

    @Test
    public void testGetWeatherStringFromLocalDate() {
        String weatherString = DateUtil.getWeatherString(LocalDate.of(2000, 1, 1));
        assertEquals(weatherString, "2000-01-01");
    }

    @Test
    public void testGetWeatherStringFromLocalDateTime() {
        String weatherString = DateUtil.getWeatherString(LocalDateTime.of(2000, 1, 1, 0, 0));
        assertEquals(weatherString, "2000-01-01");
    }

    @Test
    public void testGetLocalDateFromString() {
        LocalDate localDate = DateUtil.getLocalDateFromString("2000-01-01");
        assertEquals(localDate, LocalDate.of(2000, 1, 1));
    }

    @Test
    public void testGeLocalDateFromString() {
        LocalDateTime localDateTime = DateUtil.getLocalDateTimeFromString("2000-01-01 00:00:00");
        assertEquals(localDateTime, LocalDateTime.of(2000, 1, 1, 0, 0, 0));
    }
}
