package com.weather.util;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RequestUtilTest {
    @Test
    public void testGetWeatherStringFromLocalDate() {
        String googleUtl = "https://www.google.com";
        String request = RequestUtil.getRequest(googleUtl);
        assertTrue(request.contains("google.com"));
    }
}
