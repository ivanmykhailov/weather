package com.weather;

import com.weather.service.WeatherScheduler;

import java.util.HashSet;
import java.util.Set;

public class WeatherApplication {

    public static void main(String[] args) {
        Set<String> weatherSources = new HashSet<>();
        for (String sources : args) {
            weatherSources.add(sources);
        }
        WeatherScheduler weatherScheduler = new WeatherScheduler(weatherSources);
        weatherScheduler.start();
    }

}
