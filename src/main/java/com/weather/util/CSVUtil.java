package com.weather.util;

import com.weather.model.WeatherModel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;

public class CSVUtil {
    private static Logger log = LoggerFactory.getLogger(CSVUtil.class);
    private static final String csvWeatherHeader = "Source,forecast_date,creation_date,Temperature_f,Temperature_c,City";

    public static Collection<Map<String, String>> parseCsv(String filePath) {
        Collection<Map<String, String>> result = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(new File(filePath).toPath(), StandardCharsets.UTF_8)) {

            CSVFormat csvFormat = CSVFormat.newFormat(',').withNullString("NULL").withEscape('\\').withQuote('"').withCommentMarker('#').withHeader(new String[0]);
            Iterator<CSVRecord> recordIterator = CSVParser.parse(bufferedReader, csvFormat).iterator();

            while (recordIterator.hasNext()) {
                result.add(recordIterator.next().toMap());

            }
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return result;
    }

    public static Set<WeatherModel> getAllSavedWeathers(String filePath) {
        Set<WeatherModel> weathers = new HashSet<>();
        for (Map<String, String> map : parseCsv(filePath)) {
            weathers.add(new WeatherModel(map));
        }
        return weathers;
    }

    public static void saveNewWeathers(List<WeatherModel> newWeathers, String filePath) {
        Set<WeatherModel> oldWeathers = getAllSavedWeathers(filePath);
        List<String> uniqueWeatherCSVLines = new ArrayList<>();
        for (WeatherModel model : newWeathers) {
            if (!oldWeathers.contains(model)) {
                uniqueWeatherCSVLines.add(getCSVLineFromWeatherModel(model));
            }
        }

        if (!uniqueWeatherCSVLines.isEmpty()) {
            if (!new File(filePath).exists()) {
                uniqueWeatherCSVLines.add(0, csvWeatherHeader);
            }
            FileUtil.saveNewOrAddToEndOfFile(filePath, uniqueWeatherCSVLines);
        }

    }

    public static String getCSVLineFromWeatherModel(WeatherModel weatherModel) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(weatherModel.getSource()).append(",")
                .append(weatherModel.getForecastDate()).append(",")
                .append(weatherModel.getCreationDate()).append(",")
                .append(weatherModel.getTemperatureFahrenheit()).append(",")
                .append(weatherModel.getTemperatureCelsius()).append(",")
                .append(weatherModel.getCity());
        return buffer.toString();
    }
}