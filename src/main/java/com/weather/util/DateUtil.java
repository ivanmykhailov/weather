package com.weather.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static String getWeatherString(LocalDateTime localDateTime) {
        return DateTimeFormatter.ofPattern(SystemSettingsUtil.getDateWeatherFormat()).format(localDateTime);
    }

    public static String getWeatherString(LocalDate localDate) {
        return DateTimeFormatter.ofPattern(SystemSettingsUtil.getDateWeatherFormat()).format(localDate);
    }

    public static LocalDate getLocalDateFromString(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(SystemSettingsUtil.getDateWeatherFormat()));
    }

    public static LocalDateTime getLocalDateTimeFromString(String date) {
        return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
