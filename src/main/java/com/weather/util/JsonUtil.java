package com.weather.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public class JsonUtil {
    private static Logger log = LoggerFactory.getLogger(JsonUtil.class);
//            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static String objectToJson(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return "";
    }

    public static <T> T jsonToObject(String json, Class<T> type) {
        if (Objects.isNull(json) || json.length() == 0) {
            return null;
        }
        try {
            return new ObjectMapper().readValue(json, type);
        } catch (Exception e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }
}
