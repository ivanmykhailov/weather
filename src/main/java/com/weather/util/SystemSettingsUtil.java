package com.weather.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

public class SystemSettingsUtil {
    private static Logger log = LoggerFactory.getLogger(SystemSettingsUtil.class);

    private static String openWeatherMapApiKey;
    private static String weatherStackApiKey;
    private static String dateWeatherFormat;
    private static String weatherFilePath;
    private static Collection<String> cityNameToGetWeatherFrom;

    static {
        try (InputStream input = SystemSettingsUtil.class.getClassLoader().getResourceAsStream("application.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            openWeatherMapApiKey = prop.getProperty("openweathermap.apiKey");
            weatherStackApiKey = prop.getProperty("weatherstack.apiKey");
            dateWeatherFormat = prop.getProperty("date.weather.format");
            weatherFilePath = prop.getProperty("weather.file.path");
            cityNameToGetWeatherFrom = Arrays.asList(prop.getProperty("city.name.to.get.weather.from").split(","));
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
    }

    public static String getOpenWeatherMapApiKey() {
        return openWeatherMapApiKey;
    }

    public static String getWeatherStackApiKey() {
        return weatherStackApiKey;
    }

    public static String getDateWeatherFormat() {
        return dateWeatherFormat;
    }

    public static String getWeatherFilePath() {
        return weatherFilePath;
    }

    public static Collection<String> getCityNameToGetWeatherFrom() {
        return cityNameToGetWeatherFrom;
    }
}
