package com.weather.util;

public class TemperatureUtil {
    public static Float fahrenheitToCelsius(Float fahrenheit) {
        return (fahrenheit - 32F) / 5f / 9f;
    }

    public static Float celsiusToFahrenheit(Float celsius) {
        return (celsius * 9f / 5f) + 32f;
    }
}
