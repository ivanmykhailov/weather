package com.weather.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FileUtil {
    private static Logger log = LoggerFactory.getLogger(FileUtil.class);

    public static List<String> getListStringFromFile(String filePath) {
        List<String> result = new ArrayList<>();
        try (BufferedReader br = Files.newBufferedReader(new File(filePath).toPath(), StandardCharsets.UTF_8)) {
            for (String line = null; (line = br.readLine()) != null; ) {
                result.add(line);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
        return result;
    }

    public static void saveToFile(String file, String fileString) {
        try (PrintWriter out = new PrintWriter(file)) {
            out.println(fileString);
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
    }

    public static void saveToFile(String file, List<String> data) {
        try (PrintWriter out = new PrintWriter(file)) {
            for (String line : data) {
                out.println(line);
            }
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
    }

    public static void saveNewOrAddToEndOfFile(String file, Collection<String> data) {
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            for (String line : data) {
                out.println(line);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
        }
    }
}
