package com.weather.model;

import java.util.Map;
import java.util.Objects;

public class WeatherModel {
    //Source
    private String source;
    //forecast_date
    private String forecastDate;
    //creation_date
    private String creationDate;
    //Temperature_f
    private Float temperatureFahrenheit;
    //Temperature_c
    private Float temperatureCelsius;
    private String city;

    public WeatherModel() {
    }

    public WeatherModel(String source, String forecastDate, String creationDate, Float temperatureFahrenheit, Float temperatureCelsius, String city) {
        this.source = source;
        this.forecastDate = forecastDate;
        this.creationDate = creationDate;
        this.temperatureFahrenheit = temperatureFahrenheit;
        this.temperatureCelsius = temperatureCelsius;
        this.city = city;
    }

    public WeatherModel(Map<String, String> map) {
        this.source = map.get("Source");
        this.forecastDate = map.get("forecast_date");
        this.creationDate = map.get("creation_date");
        this.temperatureFahrenheit = Float.parseFloat(map.get("Temperature_f"));
        this.temperatureCelsius = Float.parseFloat(map.get("Temperature_c"));
        this.city = map.get("City");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }


    public String getForecastDate() {
        return forecastDate;
    }

    public void setForecastDate(String forecastDate) {
        this.forecastDate = forecastDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Float getTemperatureFahrenheit() {
        return temperatureFahrenheit;
    }

    public void setTemperatureFahrenheit(Float temperatureFahrenheit) {
        this.temperatureFahrenheit = temperatureFahrenheit;
    }

    public Float getTemperatureCelsius() {
        return temperatureCelsius;
    }

    public void setTemperatureCelsius(Float temperatureCelsius) {
        this.temperatureCelsius = temperatureCelsius;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherModel that = (WeatherModel) o;
        return Objects.equals(source, that.source) &&
                Objects.equals(forecastDate, that.forecastDate) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, forecastDate, creationDate, city);
    }
}