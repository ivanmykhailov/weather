package com.weather.model.openWeather;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class WeatherDataModel {
    private Long dt;
    private Map<String, Object> main;
    private List<Weather> weather;
    private Map<String, Object> clouds;
    private Map<String, Object> wind;
    private Map<String, Object> rain;
    private Map<String, Object> snow;
    private Map<String, Object> sys;
    @JsonProperty("dt_txt")
    private String dtTxt;

    public WeatherDataModel() {
    }

    public Long getDt() {
        return dt;
    }

    public void setDt(Long dt) {
        this.dt = dt;
    }

    public Map<String, Object> getMain() {
        return main;
    }

    public void setMain(Map<String, Object> main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Map<String, Object> getClouds() {
        return clouds;
    }

    public void setClouds(Map<String, Object> clouds) {
        this.clouds = clouds;
    }

    public Map<String, Object> getWind() {
        return wind;
    }

    public void setWind(Map<String, Object> wind) {
        this.wind = wind;
    }

    public Map<String, Object> getRain() {
        return rain;
    }

    public void setRain(Map<String, Object> rain) {
        this.rain = rain;
    }

    public Map<String, Object> getSnow() {
        return snow;
    }

    public void setSnow(Map<String, Object> snow) {
        this.snow = snow;
    }

    public Map<String, Object> getSys() {
        return sys;
    }

    public void setSys(Map<String, Object> sys) {
        this.sys = sys;
    }


    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }
}
