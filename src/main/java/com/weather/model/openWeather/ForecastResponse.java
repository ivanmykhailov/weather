package com.weather.model.openWeather;

import java.util.List;

public class ForecastResponse {
    private String cod;
    private Float message;
    private Integer cnt;
    private City city;
    private List<WeatherDataModel> list;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Float getMessage() {
        return message;
    }

    public void setMessage(Float message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<WeatherDataModel> getList() {
        return list;
    }

    public void setList(List<WeatherDataModel> list) {
        this.list = list;
    }



}