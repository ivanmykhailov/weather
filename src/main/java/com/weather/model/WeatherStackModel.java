package com.weather.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class WeatherStackModel {
    private Request request;
    private Location location;
    private Current current;

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public static class Request {
        private String type;
        private String query;
        private String language;
        private String unit;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getQuery() {
            return query;
        }

        public void setQuery(String query) {
            this.query = query;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    public static class Location {
        private String name;
        private String country;
        private String region;
        private String lat;
        private String lon;
        @JsonProperty("timezone_id")
        private String timeZoneId;
        private String localtime;
        @JsonProperty("localtime_epoch")
        private String localTimeEpoch;
        @JsonProperty("utc_offset")
        private String utcOffset;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public String getTimeZoneId() {
            return timeZoneId;
        }

        public void setTimeZoneId(String timeZoneId) {
            this.timeZoneId = timeZoneId;
        }

        public String getLocaltime() {
            return localtime;
        }

        public void setLocaltime(String localtime) {
            this.localtime = localtime;
        }

        public String getLocalTimeEpoch() {
            return localTimeEpoch;
        }

        public void setLocalTimeEpoch(String localTimeEpoch) {
            this.localTimeEpoch = localTimeEpoch;
        }

        public String getUtcOffset() {
            return utcOffset;
        }

        public void setUtcOffset(String utcOffset) {
            this.utcOffset = utcOffset;
        }
    }

    public static class Current {
        @JsonProperty("observation_time")
        private String observationTime;
        private Float temperature;
        @JsonProperty("weather_code")
        private Integer weatherCode;
        @JsonProperty("weather_icons")
        private Set<String> weatherIcons;
        @JsonProperty("weather_descriptions")
        private Set<String> weatherDescriptions;
        @JsonProperty("wind_speed")
        private Integer windSpeed;
        @JsonProperty("wind_degree")
        private Integer windDegree;
        @JsonProperty("wind_dir")
        private String windDir;
        private Integer pressure;
        private Integer precip;
        private Integer humidity;
        @JsonProperty("cloudcover")
        private Integer cloudCover;
        @JsonProperty("feelslike")
        private Integer feelsLike;
        @JsonProperty("uv_index")
        private Integer uvIndex;
        private Integer visibility;
        @JsonProperty("is_day")
        private String isDay;

        public String getObservationTime() {
            return observationTime;
        }

        public void setObservationTime(String observationTime) {
            this.observationTime = observationTime;
        }

        public Float getTemperature() {
            return temperature;
        }

        public void setTemperature(Float temperature) {
            this.temperature = temperature;
        }

        public Integer getWeatherCode() {
            return weatherCode;
        }

        public void setWeatherCode(Integer weatherCode) {
            this.weatherCode = weatherCode;
        }

        public Set<String> getWeatherIcons() {
            return weatherIcons;
        }

        public void setWeatherIcons(Set<String> weatherIcons) {
            this.weatherIcons = weatherIcons;
        }

        public Set<String> getWeatherDescriptions() {
            return weatherDescriptions;
        }

        public void setWeatherDescriptions(Set<String> weatherDescriptions) {
            this.weatherDescriptions = weatherDescriptions;
        }

        public Integer getWindSpeed() {
            return windSpeed;
        }

        public void setWindSpeed(Integer windSpeed) {
            this.windSpeed = windSpeed;
        }

        public Integer getWindDegree() {
            return windDegree;
        }

        public void setWindDegree(Integer windDegree) {
            this.windDegree = windDegree;
        }

        public String getWindDir() {
            return windDir;
        }

        public void setWindDir(String windDir) {
            this.windDir = windDir;
        }

        public Integer getPressure() {
            return pressure;
        }

        public void setPressure(Integer pressure) {
            this.pressure = pressure;
        }

        public Integer getPrecip() {
            return precip;
        }

        public void setPrecip(Integer precip) {
            this.precip = precip;
        }

        public Integer getHumidity() {
            return humidity;
        }

        public void setHumidity(Integer humidity) {
            this.humidity = humidity;
        }

        public Integer getCloudCover() {
            return cloudCover;
        }

        public void setCloudCover(Integer cloudCover) {
            this.cloudCover = cloudCover;
        }

        public Integer getFeelsLike() {
            return feelsLike;
        }

        public void setFeelsLike(Integer feelsLike) {
            this.feelsLike = feelsLike;
        }

        public Integer getUvIndex() {
            return uvIndex;
        }

        public void setUvIndex(Integer uvIndex) {
            this.uvIndex = uvIndex;
        }

        public Integer getVisibility() {
            return visibility;
        }

        public void setVisibility(Integer visibility) {
            this.visibility = visibility;
        }

        public String getIsDay() {
            return isDay;
        }

        public void setIsDay(String isDay) {
            this.isDay = isDay;
        }
    }
}
