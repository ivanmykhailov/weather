package com.weather.service;

public class WeatherSourceFactory {
    public WeatherSource getWeatherSource(String sourceName) {
        if (sourceName.equals("openweathermap")) {
            return new OpenWeatherSourceServiceImpl();
        } else if (sourceName.equals("weatherstack")) {
            return new WeatherStackSourceServiceImpl();
        }
        return null;
    }
}
