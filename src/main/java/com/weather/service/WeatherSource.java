package com.weather.service;

import com.weather.model.WeatherModel;
import com.weather.util.CSVUtil;
import com.weather.util.DateUtil;
import com.weather.util.SystemSettingsUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public interface WeatherSource {
    List<WeatherModel> getWeathers(String cityName);

    void saveTodayWeather();

    default void saveTodayWeather(String serviceName) {
        if (checkIfAlreadySavedToday(serviceName)) {
            return;
        }
        List<WeatherModel> weatherModels = new ArrayList<>();
        for (String cityName : SystemSettingsUtil.getCityNameToGetWeatherFrom()) {
            weatherModels.addAll(getWeathers(cityName));
        }
        if (!weatherModels.isEmpty()) {
            CSVUtil.saveNewWeathers(weatherModels, SystemSettingsUtil.getWeatherFilePath());
        }
    }

    default Boolean checkIfAlreadySavedToday(String serviceName) {
        Set<WeatherModel> weatherModels = CSVUtil.getAllSavedWeathers(SystemSettingsUtil.getWeatherFilePath());
        String todayDate = DateUtil.getWeatherString(LocalDate.now());
        for (WeatherModel model : weatherModels) {
            if (model.getSource().equals(serviceName) && model.getCreationDate().equals(todayDate)) {
                return true;
            }
        }
        return false;
    }


}
