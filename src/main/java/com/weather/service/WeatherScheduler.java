package com.weather.service;

import com.weather.util.CSVUtil;

import java.util.Set;

public class WeatherScheduler extends Thread {

    private Set<String> weatherSources;

    public WeatherScheduler(Set<String> weatherSources) {
        this.weatherSources = weatherSources;
    }

    @Override
    public void run() {
        while (true) {
            for (String source : weatherSources) {
                WeatherSource weatherSource = new WeatherSourceFactory().getWeatherSource(source);
                weatherSource.saveTodayWeather();
            }
            try {
                Thread.sleep(1000 * 60 * 60 * 3);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
