package com.weather.service;

import com.weather.model.WeatherModel;
import com.weather.model.openWeather.ForecastResponse;
import com.weather.model.openWeather.WeatherDataModel;
import com.weather.util.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class OpenWeatherSourceServiceImpl implements WeatherSource {
    private static final String serviceName = "openweathermap";
    private static final String apiUrl = "https://api.openweathermap.org/data/2.5/forecast?units=metric&appid=%s&q=%s";

    @Override
    public List<WeatherModel> getWeathers(String cityName) {
        ForecastResponse forecastResponse
                = JsonUtil.jsonToObject(
                RequestUtil.getRequest(
                        String.format(Locale.ROOT,
                                apiUrl,
                                SystemSettingsUtil.getOpenWeatherMapApiKey(),
                                cityName)
                ), ForecastResponse.class);

        return getWeatherModelFromOpenWeatherForecast(cityName, forecastResponse);

    }

    @Override
    public void saveTodayWeather() {
        saveTodayWeather(serviceName);
    }

    private List<WeatherModel> getWeatherModelFromOpenWeatherForecast(String cityName, ForecastResponse forecastResponse) {
        List<WeatherModel> weatherModels = new ArrayList<>();
        String date = DateUtil.getWeatherString(LocalDate.now());

        LocalDate previousDay = null;
        for (WeatherDataModel weatherDataModel : forecastResponse.getList()) {
            LocalDateTime localDateTime = DateUtil.getLocalDateTimeFromString(weatherDataModel.getDtTxt());

            if (Objects.isNull(previousDay) || !previousDay.equals(localDateTime.toLocalDate())) {
                Float temperatureCelsius = Float.parseFloat(weatherDataModel.getMain().get("temp").toString());
                Float temperatureFahrenheit = TemperatureUtil.celsiusToFahrenheit(temperatureCelsius);
                weatherModels.add(new WeatherModel(serviceName, DateUtil.getWeatherString(localDateTime), date, temperatureFahrenheit, temperatureCelsius, cityName));
            }
            previousDay = localDateTime.toLocalDate();
            if (weatherModels.size() == 3) {
                break;
            }
        }
        return weatherModels;
    }
}
