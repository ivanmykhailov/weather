package com.weather.service;

import com.weather.model.WeatherModel;
import com.weather.model.WeatherStackModel;
import com.weather.model.openWeather.Weather;
import com.weather.util.*;

import java.time.LocalDate;
import java.util.*;

public class WeatherStackSourceServiceImpl implements WeatherSource {
    private static final String serviceName = "weatherstack";
    private static final String apiUrl = "http://api.weatherstack.com/current?access_key=%s&query=%s";

    @Override
    public List<WeatherModel> getWeathers(String cityName) {
        WeatherStackModel weatherStackModel
                = JsonUtil.jsonToObject(
                RequestUtil.getRequest(
                        String.format(Locale.ROOT,
                                apiUrl,
                                SystemSettingsUtil.getWeatherStackApiKey(),
                                cityName)
                ), WeatherStackModel.class);

        return getWeatherModelFromWeatherStack(cityName, weatherStackModel);
    }

    @Override
    public void saveTodayWeather() {
        saveTodayWeather(serviceName);
    }


    private List<WeatherModel> getWeatherModelFromWeatherStack(String cityName, WeatherStackModel weatherStackModel) {
        List<WeatherModel> weatherModels = new ArrayList<>();

        String date = DateUtil.getWeatherString(LocalDate.now());
        Float temperatureCelsius = weatherStackModel.getCurrent().getTemperature();
        Float temperatureFahrenheit = TemperatureUtil.celsiusToFahrenheit(temperatureCelsius);

        weatherModels.add(new WeatherModel(serviceName, date, date, temperatureFahrenheit, temperatureCelsius, cityName));
        return weatherModels;
    }
}
